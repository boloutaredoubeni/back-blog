'use strict';

const http = require('http');
const path = require('path');


const express = require('express');
const json = require('express-json');
const mongoose = require('mongoose');

const PORT = process.env.PORT || 3000;

const app = express();
  // serve static assets
app.use(express.static(path.join(__dirname, 'public')));
app.use(json());
  // Set the templates
app.engine('jade', require('jade').__express);
app.set('views', './views');
  // Include scripts
app.use('/scripts/vendor', express.static(path.join(__dirname, '/node_modules/')));

app.locals.pretty = true;

mongoose.connect('mongodb://localhost/backblog');

const PostSchema = new mongoose.Schema({
  id: Number,
  pubDate: Date,
  title: String,
  content: String
});

const Post = mongoose.model('posts', PostSchema);

const CommentSchema = new mongoose.Schema({

});

const Comment = mongoose.model('comments', CommentSchema);


app.get('/', (req, res) => {
  Post.find({}, (err, results) => {
    if (err) {
      throw err;
    }
    res.render('index.jade', { posts: JSON.stringify(results) });
  });
});


// Handle all posts
app.get('/posts', (req, res) => {
  Post.find({}, (err, results) => {
    if (err) {
      throw err;
    }
    res.json(results);
  });
});

app.post('/posts', (req, res) => {
  new Post({
    id: req.body.id,
    pubDate: req.body.pubDate,
    title: req.body.title,
    content: req.body.content
  }).save((err, results) => {
      if (err) {
        throw err;
      }
      res.send(results);
    });
  //Posts.insert(req.body, (result) => {
  //  res.json(result);
  //});
});

app.get('/posts/:id/comments', function(req, res) {
  comments.find(
    { postId: parseInt(req.params.id, 10) },
    function (err, results) {
      res.json(results);
    }
  );
});

app.get('/posts/:id/comments', function(req, res) {
  comments.insert(req.body, function (err, result) {
    res.json(result);
  });
});


http.createServer(app)
  .listen(PORT, () => {
    console.log('Listening at http://localhost:' + PORT);
  });
